import React from 'react';
import PropTypes from 'prop-types';

// mui
import {
  Dialog, DialogContent, DialogContentText, DialogActions, Button, DialogTitle,
} from '@mui/material';

function Confirm(props) {
  const {
    open, handleConfirm, handleClose, status,
  } = props;

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title">
        Are you sure want to
        {' '}
        {status}
        {' '}
        this note ?
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          Click yes to confirm.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleClose}>
          Cancel
        </Button>
        <Button onClick={handleConfirm} autoFocus>
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}

Confirm.propTypes = {
  open: PropTypes.bool.isRequired,
  status: PropTypes.string.isRequired,
  handleConfirm: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default Confirm;
