import React from 'react';
import PropTypes from 'prop-types';

// mui
import {
  IconButton,
  Card,
  CardContent,
  Typography,
  CardActions,
  useMediaQuery,
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import ArchiveIcon from '@mui/icons-material/Archive';
import UnarchiveIcon from '@mui/icons-material/Unarchive';

// Highlighter
import Highlighter from 'react-highlight-words';

// utils
import { parseDate } from '../utils';

function NoteCard(props) {
  const {
    note, handleClickOpen, search,
  } = props;
  const mobileView = useMediaQuery('(max-width:562px)');
  return (
    <Card sx={{
      minWidth: mobileView ? '100%' : 250, maxWidth: mobileView ? '100%' : 250, margin: 1, display: 'flex', flexDirection: 'column', justifyContent: 'space-between',
    }}
    >
      <CardContent>
        <Typography variant="h5" component="div" sx={{ fontWeight: 'bold', overflowWrap: 'break-word' }}>
          <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={[search]}
            autoEscape
            textToHighlight={note.title}
          />
        </Typography>
        <Typography variant="subtitle1" sx={{ mb: 1.5, fontSize: '0.7rem', overflowWrap: 'break-word' }} color="text.secondary">
          { parseDate(note.createdAt) }
        </Typography>
        <Typography variant="body2">
          <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={[search]}
            autoEscape
            textToHighlight={note.body}
          />
        </Typography>
      </CardContent>
      <CardActions>
        <IconButton onClick={() => handleClickOpen(note.id, 'delete')} color="error">
          <DeleteIcon />
        </IconButton>
        <IconButton onClick={() => handleClickOpen(note.id, note.archived ? 'unarchive' : 'archive')} color="primary">
          {!note.archived ? <ArchiveIcon /> : <UnarchiveIcon /> }
        </IconButton>
      </CardActions>
    </Card>
  );
}

NoteCard.propTypes = {
  note: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string,
    archived: PropTypes.bool,
    createdAt: PropTypes.string,
  }).isRequired,
  handleClickOpen: PropTypes.func.isRequired,
  search: PropTypes.string.isRequired,
};

export default NoteCard;
