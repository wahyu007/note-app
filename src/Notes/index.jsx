import React, { useState } from 'react';
import PropTypes from 'prop-types';

// mui
import {
  Box,
  useMediaQuery, Typography,
} from '@mui/material';

// component
import NoteCard from './NoteCard';
import Confirm from './Confirm';

function Notes(props) {
  const {
    notes, handleArchived, handleDelete, search,
  } = props;
  const [open, setOpen] = useState(false);
  const [id, setId] = useState('');
  const [status, setStatus] = useState('');

  const mobileView = useMediaQuery('(max-width:562px)');

  const handleClickOpen = (i, stat) => {
    setId(i);
    setStatus(stat);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleConfirm = () => {
    if (status === 'delete') {
      handleDelete(id);
    }
    if (status === 'archive' || status === 'unarchive') {
      handleArchived(id);
    }
    handleClose();
  };

  return (
    <Box sx={{
      mt: 1, display: 'flex', flexWrap: 'wrap', paddingRight: mobileView && 2,
    }}
    >
      {notes.length === 0 && (
        <Box sx={{ height: '10rem', width: '100%' }}>
          <Typography sx={{ width: '100%', marginTop: '5rem' }} variant="h6" component="h6" align="center" gutterBottom>Rows is empty</Typography>
        </Box>
      )}

      {notes.map((note) => (
        <NoteCard
          key={note.id}
          note={note}
          handleClickOpen={handleClickOpen}
          search={search}
        />
      ))}
      <Confirm
        open={open}
        status={status}
        handleConfirm={handleConfirm}
        handleClose={handleClose}
      />
    </Box>
  );
}

Notes.propTypes = {
  notes: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string,
    archived: PropTypes.bool,
    createdAt: PropTypes.string,
  })).isRequired,
  handleArchived: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  search: PropTypes.string.isRequired,
};

export default Notes;
