import React, { useState } from 'react';
import PropTypes from 'prop-types';

// mui
import {
  Box, Button, Dialog, DialogTitle, DialogContent, TextField, DialogActions,
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

const buttons = [
  {
    label: 'All',
    value: 'all',
  },
  {
    label: 'Archive',
    value: 'archive',
  },
];

const inputs = [
  {
    id: 'title',
    label: 'Title',
    value: 'title',
    variant: 'standard',
    type: 'text',
    fullWidth: true,
    multiline: false,
  },
  {
    id: 'content',
    label: 'Content',
    value: 'content',
    variant: 'standard',
    type: 'text',
    fullWidth: true,
    multiline: true,
  },
];

function NavBar(props) {
  const { setMenu, addNote } = props;
  const [selected, setSelected] = useState('all');
  const [open, setOpen] = useState(false);
  const [label, setLabel] = useState('Title');
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [error, setError] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const newDate = new Date();
    addNote({
      id: Date.now(), title, body, archived: false, createdAt: newDate.toISOString(),
    });
    handleClose();
    setTitle('');
    setBody('');
    setLabel('Title');
  };

  function handleMenu(val) {
    setSelected(val);
    setMenu(val);
  }

  function handleChange(name, val) {
    if (name === 'title') {
      setError(false);
      if (val.length === 0) {
        setLabel('Title');
        setTitle(val);
      } else if (val.length <= 50) {
        setTitle(val);
        setLabel(`Character left ${50 - val.length}`);
      } else {
        setError(true);
      }
    } else if (name === 'content') {
      setBody(val);
    }
  }

  return (
    <>
      <Box sx={{
        mt: 1, mx: 1, justifyContent: 'space-between', display: 'flex',
      }}
      >
        <Box>
          {buttons.map((btn) => (
            <Button key={btn.value} onClick={() => handleMenu(btn.value)} sx={{ textTransform: 'none', minWidth: '100px' }} variant={selected === btn.value ? 'contained' : 'text'} startIcon={btn.icon || null}>{btn.label}</Button>
          ))}
        </Box>
        <Box>
          <Button onClick={handleClickOpen} sx={{ textTransform: 'none', minWidth: '100px' }} startIcon={<AddIcon />}>Create</Button>
        </Box>
      </Box>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Create</DialogTitle>
        <Box
          component="form"
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <DialogContent>
            {
              inputs.map((input) => (
                <TextField
                  key={input.id}
                  autoFocus
                  margin="dense"
                  id={input.id}
                  label={input.id === 'title' ? label : input.label}
                  type={input.type}
                  placeholder={`Type ${input.label}`}
                  variant={input.variant}
                  onChange={(e) => handleChange(input.value, e.target.value)}
                  value={input.id === 'title' ? title : body}
                  color={error ? 'error' : null}
                  fullWidth={input.fullWidth}
                  multiline={input.multiline}
                  required
                />
              ))
            }
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button type="submit">Save</Button>
          </DialogActions>
        </Box>
      </Dialog>
    </>
  );
}

NavBar.propTypes = {
  setMenu: PropTypes.func.isRequired,
  addNote: PropTypes.func.isRequired,
};

export default NavBar;
