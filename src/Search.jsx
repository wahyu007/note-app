import React, { useState } from 'react';
import PropTypes from 'prop-types';

// mui
import {
  Box, Paper, IconButton, InputBase,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import ClearIcon from '@mui/icons-material/Clear';

function Search(props) {
  const { handleSearch } = props;
  const [search, setSearch] = useState('');

  const handleChange = (e) => {
    setSearch(e);
    handleSearch(e);
  };

  return (
    <Box sx={{ mx: 1 }}>
      <Paper
        sx={{
          p: '2px 4px', display: 'flex', alignItems: 'center',
        }}
      >
        <InputBase
          sx={{ ml: 1, flex: 1 }}
          placeholder="Search"
          inputProps={{ 'aria-label': 'search' }}
          onChange={(e) => handleChange(e.target.value)}
          value={search}
        />
        <IconButton onClick={() => handleChange('')} sx={{ p: '10px' }} aria-label="search" disabled={search === ''} color="primary">
          { search === '' ? <SearchIcon /> : <ClearIcon /> }
        </IconButton>
      </Paper>
    </Box>
  );
}

Search.propTypes = {
  handleSearch: PropTypes.func.isRequired,
};

export default Search;
