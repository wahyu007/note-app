import React, { useEffect, useState } from 'react';

// mui
import { Grid } from '@mui/material';

// component
import Notes from './Notes';
import Search from './Search';
import NavBar from './NavBar';

// utils
import { getInitialData } from './utils';

function App() {
  const [archived, setArchived] = useState(false);
  const [notes, setNotes] = useState([]);
  const [search, setSearch] = useState('');

  const setMenu = (val) => {
    setArchived(val !== 'all');
  };

  const addNote = (val) => {
    setNotes((pre) => ([...pre, val]));
  };

  const handleArchived = (id) => {
    setNotes((pre) => pre.map((el) => (el.id === id ? { ...el, archived: !el.archived } : el)));
  };

  const handleDelete = (id) => {
    setNotes((pre) => pre.filter((el) => el.id !== id));
  };

  const handleSearch = (val) => {
    setSearch(val);
  };

  useEffect(() => {
    const data = getInitialData();
    setNotes(data);
  }, []);

  return (
    <Grid container sx={{ justifyContent: 'center', maxWidth: 800, margin: '0 auto' }}>
      <Grid sx={{ width: '100%' }}>
        <Search handleSearch={handleSearch} />
        <NavBar setMenu={setMenu} addNote={addNote} />
        <Notes
          notes={notes.filter((el) => {
            if (search !== '') {
              const sTitle = el.title.toLowerCase();
              const sBody = el.body.toLowerCase();
              const sSearch = search.toLowerCase();
              return el.archived === archived
                && (sTitle.includes(sSearch) || sBody.includes(sSearch));
            }
            return el.archived === archived;
          })}
          handleArchived={handleArchived}
          handleDelete={handleDelete}
          search={search}
        />
      </Grid>
    </Grid>
  );
}

export default App;
